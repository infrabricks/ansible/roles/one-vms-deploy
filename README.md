# Open Nebula deploy VMs
Ansible role to deploy VMs on Open Nebula host (KVM).

## Requirements

* Ansible >= 4

## OS

* All OS supported on installed Open Nebula hosts.

## Role Variables

Variable name      | Variable description   | Type    | Default | Required
---                | ---                    | ---     | ---     | ---
one_vm_disk_size   | VM disk size           | string  | Yes     | Yes
one_vm_memory      | VM RAM size            | string  | Yes     | Yes
one_vm_vcpu        | VM vcpu number         | integer | Yes     | Yes
one_vm_count       | Number of VM to deploy | integer | Yes     | Yes
one_vm_name        | VM names               | string  | No      | Yes
one_vm_template_id | VM template ID         | integer | No      | Yes
one_vm_net_id      | VM network ID          | integer | No      | Yes

## Dependencies

* [pyone](https://pypi.org/project/pyone/)

## Playbook Example

```
- name: Open Nebula deploy VMs
  hosts: localhost
  vars_files:
    - vars/main.yml
    - vars/secret.yml
  tasks:
    - name: Include Open Nebula VMs deploy role
      ansible.builtin.include_role:
        name: one_vms_deploy
      vars:
        one_vm_count: 1
        one_vm_template_id: templateid
        one_vm_name: testansible
        one_vm_disk_size: 20 GB
        one_vm_net_id: networkid
        one_vm_net_name: networkname
```

## License

GPL v3

## Links

* [Ansible documentation - Creates or terminates OpenNebula instances](https://docs.ansible.com/ansible/latest/collections/community/general/one_vm_module.html#ansible-collections-community-general-one-vm-module)
* [Open Nebula documentation - Automation Tools Integration - Ansible](https://docs.opennebula.io/6.6/integration_and_development/automation_tools_integration/ansible.html?highlight=ansible)

## Author Information

* [Stephane Paillet](mailto:spaillet@ethicsys.fr)
